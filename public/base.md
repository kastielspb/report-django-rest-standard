class: center, middle

# Django Rest Framework. Standards.

---

## Что такое Rest?

`REST (REpresentational State Transfer)` – это стиль архитектуры программного обеспечения для распределенных систем, таких как `World Wide Web`, который, как правило, используется для построения веб-служб.

Для веб-служб, построенных с учётом `REST`, применяют термин `«RESTful»`.

`Профит`:
- Масштабируемость взаимодействия компонентов
- Общность интерфейсов
- Независимое внедрение компонентов
- т.д.

Данные в `REST` должны передаваться в виде небольшого количества стандартных форматов (например `HTML`, `XML`, `JSON`) с указанием соответствующего `хедерa` (например `Content-Type: application/json`).

---

## Django REST Framework

`Django REST framework` — удобный инструмент для работы с `REST` основанный на идеологии фреймворка Django.

#### Основные компоненты:

1. `rest_framework.generics.GenericAPIView` - аналог `django.views.generic.View`.
2. `rest_framework.serializers.Serializer` - аналог `django.forms.Form`.

Оба компонента, так же как их аналоги, имеют множество наследников, которые заточены под разные задачи.

---

## Settings:

```bash
pipenv install djangorestframework
```

```python
# apps/settings/default.py
INSTALLED_APPS = PROJECT_APPS + [
    ...
    'rest_framework',
    ...
]
...
REST_FRAMEWORK = {
    # Some rest framework settings
}
```

```python
# app/urls.py
urlpatterns = [
    ...
    url(r'^api-auth/', include('rest_framework.urls'))
]
```
---

#### Пример:

```python
# apps/some_app/serializer.py
from rest_framework import serializers

from .models import SomeModel

__all__ = ('SomeSerializer', )

class SomeSerializer(serializers.ModelSerializer):
    field2 = serializers.CharField(source='filed3')

    class Meta:
        fields = ('field1', 'field2')
        model = SomeModel
```

```python
# apps/some_app/views.py
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny

from .models import SomeModel
from .serializers import SomeSerializer

__all__ = ('SomeListAPIView', )

class SomeListAPIView(ListAPIView):
    permission_classes = (AllowAny,)
    queryset = SomeModel.objects.all()
    serializer_class = SomeSerializer
```

<!-- ---

## Common HTTP methods

- GET - чтение данных ресурса.

- HEAD - запрашивает заголовки, идентичные тем, что возвращаются с помощью `GET`. Такой запрос может быть выполнен перед загрузкой большого ресурса, например, для экономии пропускной способности.

- POST - используется для создания новых ресурсов.

- PUT - заменяет все текущие представления ресурса данными запроса.

- PATCH - частичного изменения ресурса данными запроса.

- DELETE - удаляет указанный ресурс.

- OPTIONS - используется для описания параметров соединения с ресурсом. -->

---

## Standards & package

#### Represented by Alex Tkachenko.

Link: https://gitlab.com/preusx/development-documentation-draft

##### Package:

Link: https://gitlab.com/kastielspb/django-awesome-standards

---

## Standards settings

```bash
pipenv install djangorestframework-camel-case
pipenv install git+https://gitlab.com/kastielspb/django-awesome-standards.git#egg=django-awesome-standards
```

```python
# apps/settings/default.py
INSTALLED_APPS = PROJECT_APPS + [
    ...
    'rest_framework',
    'standards',
    ...
]
...
REST_FRAMEWORK = {
    'DEFAULT_METADATA_CLASS': 'standards.drf.metadata.FieldsetMetadata',
    'DEFAULT_PARSER_CLASSES': (
        'djangorestframework_camel_case.parser.CamelCaseFormParser',
        'djangorestframework_camel_case.parser.CamelCaseMultiPartParser',
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
        'djangorestframework_camel_case.render.CamelCaseBrowsableAPIRenderer',
    ),
    'EXCEPTION_HANDLER': 'standards.drf.handlers.exception_handler',
}
```

---

#### Пример:

```python
# apps/some_app/serializer.py
from rest_framework import serializers
from standards.drf.serializers import ModelSerializer

from .models import SomeModel


class SomeSerializer(ModelSerializer):
    field2 = serializers.CharField(source='filed3')

    class Meta:
        fields = ('field1', 'field2')
        model = SomeModel
```

```python
# apps/some_app/views.py
from rest_framework.permissions import AllowAny
from standards.drf.pagination import limitoffset_pagination
from standards.drf.views import ListAPIView

from .models import SomeModel
from .serializers import SomeSerializer


class SomeListAPIView(ListAPIView):
    pagination_class = limitoffset_pagination(page_size=10)
    permission_classes = (AllowAny,)
    queryset = SomeModel.objects.all()
    serializer_class = SomeSerializer
```

---

## common.quering.API
### Response

Standard:
```json
{
  "code": 200,
  "data": {
    "item": {}
  }
}

or

{
  "code": 200,
  "data": {
      "items": []
  }
}
```

Realization module:
```python
standards.drf.views.RetrieveAPIView
standards.drf.views.ListAPIView
```

---

## common.quering.API
### Paginated response

Standard:
```json
{
  "code": 200,
  "data": {
    "items": [],
    "pagination": {
      "limit": 20,
      "offset": 0,
      "total": 43095
    }
  }
}
```

Realization module:
```python
standards.drf.pagination.limitoffset_pagination
standards.drf.pagination.pagenumber_pagination
```

---

## common.quering.Entity
### Entity

Standard:
```json
{
  "id": "entity-identifier-that-is-unique-inside-one-type",
  "caption": "Verbose representation of the entity object",
  "type": "EntityObjectModel",
  "props": {
    "propertyName": "Property value"
  }
}
```

Realization module:
```python
standards.drf.serializers.EntityModelSerializer
```

---

## common.quering.Request
### Request

Standard:
```json
{
  "singular": {
    "id": 10, "title": "some"
  },
  "multiple": [
    {"id": 10, "title": "some"},
    {"title": "another"},
    {"id": 11, "_delete": true}
  ]
}

```

Realization module:
```python
standards.drf.serializers.NestedListSerializer
```

---

## common.quering.Request
### Пример:

```python
from django.db import transaction

class SomeNestedSerializer(ModelSerializer):
    _delete = serializers.BooleanField(required=False, write_only=True)

    class Meta:
        extra_kwargs = {'id': {'read_only': False, 'required': False}}
        fields = ('id', '_delete', ...)
        list_serializer_class = NestedListSerializer

class SomeParentSerializer(ModelSerializer):
    some_field = SomeNestedSerializer(many=True)

    def update(self, instance, data):
        some_field_data = data.pop('some_field', [])
        with transaction.atomic():
            self.instance = super().update(instance, data)

            self.fields['some_field'].update(
                self.instance.some_field.all(),
                [
                    {'some_related': self.instance, **dict(item)}
                    for item in some_field_data
                ]
            )
```
---

## common.quering.Response
### RequestError

Standard:
```json
{
  "code": 400,
  "message": "Some basic message, like: 'Bad request'.",
  "errors": [{
    "message": "Form is invalid",
    "domain": "request",
    "reason": "form_value_invalid",
    "state": {
      "fieldName": [
        {"reason": "invalid_format", "message": "Field name has an invalid format"}
      ]
    }
  }]
}
```

Realization module:
```python
standards.drf.handlers.ExceptionHandler
standards.drf.handlers.ExceptionMessageHandler
```

---

## Metadata
Realization module:
```python
standards.drf.metadata.FieldsetMetadata
```

Response:
```json
{
    ...
    "actions": {
        "POST": {
            "type": {
                "label":"Type", "type":"choice", "required":true, "read_only":false,
                "choices":[{"value":1, "label":"Credit"},{"value":2, "label":"Debit"}]
            },
            "amount": {"label":"Amount", "type":"float", "required":false, "read_only":false},
        }
    },
    "filters": {
        "sorting": {
            "label":"Ordering", "type":"MultipleOrderingFilter",
            "choices":[{"value":"delivery_up","label":"Descending"},{"value":"delivery_down","label":"Ascending"}]
        }
    }
}

```

<!-- ---

## Common HTTP statuses
<div style="display: inline-block; margin: 20px 0;">
    <h4>2xx: Success</h4>
    <ul>
        <li>200: OK</li>
        <li>201: Created</li>
        <li>204: No Content</li>
    </ul>
    <h4>4xx: Client Error</h4>
    <ul>
        <li>400: Bad Request</li>
        <li>401: Unauthorized</li>
        <li>403: Forbidden</li>
        <li>404: Not Found</li>
    </ul>
</div>
<div style="display: inline-block; margin: 0 40px;">
    <h4>3xx: Redirect</h4>
    <ul>
        <li>301: Moved Permanently</li>
        <li>302: Moved Temporary</li>
        <li>307: Temporary Redirect</li>
    </ul>
    <h4>5xx: Server Error</h4>
    <ul>
        <li>500: Internal Server Error</li>
        <li>502: Bad Gateway</li>
        <li>503: Service Unavailable</li>
        <li>504: Gateway Timeout</li>
    </ul>
</div> -->

---

class: center, middle
# Спасибо за внимание!
