#!/bin/sh
trap "exit" INT TERM
trap "kill 0" EXIT

python3 $CONF_RENDERER;
nginx -g "daemon off;"